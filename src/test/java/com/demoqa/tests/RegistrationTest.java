package com.demoqa.tests;


import com.demoqa.common.BaseTestConfig;
import com.demoqa.pages.registration.RegistrationPage;
import com.demoqa.utils.DateOfBirthGenerator;
import com.demoqa.utils.EmailGenerator;
import com.demoqa.utils.PhoneNumberGenerator;
import com.demoqa.utils.RandomUsernameGenerator;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertTrue;

public class RegistrationTest extends BaseTestConfig {

    private RegistrationPage registrationPage;
    private EmailGenerator emailGenerator = new EmailGenerator();
    private DateOfBirthGenerator dateOfBirthGenerator = new DateOfBirthGenerator();
    private PhoneNumberGenerator phoneNumberGenerator = new PhoneNumberGenerator();
    private RandomUsernameGenerator randomStringGenerator = new RandomUsernameGenerator();
    private static final String[] listOfUsers = {"Jan van Dam", "Chack Norris", "Klark n Kent", "John Daw", "Bat Man", "Tim Los", "Dave o Core", "Pay Pal", "Lazy Cat", "Jack & Johnes"};


    @Test
    public void registerNewUserWithCorrectData() {
        List<String> users = new LinkedList<>(Arrays.asList(listOfUsers));
        Collections.shuffle(users);

        registrationPage = new RegistrationPage(driver);
        assertTrue("It looks like we were redirect to other page " + driver.getCurrentUrl(), registrationPage.getCurrentUrl().equals("http://demoqa.com/registration/"));
        for (int i = 0; i <= 4; i++) {
            String password = randomStringGenerator.getRandomString(10);
            registrationPage
                    .enterFirstAndLastName(users.get(i))
                    .selectRandomMaritalStatus()
                    .selectRandomHobie(new Random().nextInt(2) + 1)
                    .selectRandomCountry()
                    .selectDateOfBirth(dateOfBirthGenerator.generateRandomDateOfBirth())
                    .enterPhoneNumber(phoneNumberGenerator.getRandomNumber(10))
                    .enterUsername(randomStringGenerator.getRandomString(20))
                    .enterEmail(emailGenerator.generateRandomEmail(18))
                    .enterPassword(password)
                    .confirmPassword(password)
                    .submitRegistrationForm()
                    .waitForRegistrationSuccessMassageToBeDisplayed();
            assertTrue("Text [Thank you for your registration] is not displayed after registration for user " + users.get(i), registrationPage.isRegistrationSuccessMessageDisplayed());
            users.remove(i);
            navigateToBaseUrl(); //to clear form and registration message
        }
        System.out.println("Remaining users: ");
        users.forEach(System.out::println); //remaining users

    }

}
