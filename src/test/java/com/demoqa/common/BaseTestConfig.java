package com.demoqa.common;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.awt.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BaseTestConfig {

    protected static WebDriver driver;
    private String baseUrl = "http://demoqa.com/registration";

    public static WebDriver getDriver() {
        return driver;
    }

    @Before
    public void openBrowser() {
        ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();
        setupTimeouts();
        navigateToBaseUrl();
        maximizeWindow();
    }


    @After
    public void createReportAndCloseBrowser() throws IOException {
        driver.quit();
    }

    private void maximizeWindow() {
        Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize().width, Toolkit.getDefaultToolkit().getScreenSize().height);
        driver.manage().window().setSize(screenSize);
    }

    private void setupTimeouts() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
    }

    protected void navigateToBaseUrl() {
        driver.get(baseUrl);
    }
}
