package com.demoqa.pages.registration;


import com.sun.org.apache.regexp.internal.RE;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class RegistrationPage extends PageObject {

    private WebDriverWait wait = new WebDriverWait(driver, 25);


    private By maritalStatus = By.name("radio_4[]");
    private By hobby = By.name("checkbox_5[]");
    private By country = By.name("dropdown_7");

    @FindBy(xpath = "//*[@class='piereg_message'][text()='Thank you for your registration']")
    private WebElement registrationSuccessMessage;

    @FindBy(name = "first_name")
    private WebElement firstName;

    @FindBy(name = "last_name")
    private WebElement lastName;

    @FindBy(id = "mm_date_8")
    private WebElement monthOfBirth;

    @FindBy(id = "dd_date_8")
    private WebElement dayOfBirth;

    @FindBy(id = "yy_date_8")
    private WebElement yearOfBirth;

    @FindBy(id = "phone_9")
    private WebElement phoneNumber;

    @FindBy(id = "username")
    private WebElement username;

    @FindBy(name = "e_mail")
    private WebElement email;

    @FindBy(name = "password")
    private WebElement password;

    @FindBy(id = "confirm_password_password_2")
    private WebElement confirmPassword;

    @FindBy(name = "pie_submit")
    private WebElement submit;

    public RegistrationPage(WebDriver driver) {
        super(driver);
    }

    public RegistrationPage enterFirstName(String firstNameValue) {
        firstName.sendKeys(firstNameValue);
        return this;
    }

    public RegistrationPage enterLastName(String lastNameValue) {
        lastName.sendKeys(lastNameValue);
        return this;
    }

    public RegistrationPage enterFirstAndLastName(String fullUserName) {
        String spaceRegex = "\\s";
        String[] user = fullUserName.split(spaceRegex);
        enterFirstName(user[0]);
        if (user.length == 3) {
            enterLastName(user[2]);
        } else if (user.length == 2) {
            enterLastName(user[1]);
        } else throw new UnsupportedOperationException("User name is not correct or not supported " + fullUserName);
        return this;
    }

    public RegistrationPage selectRandomMaritalStatus() {
        List<WebElement> maritalStatuses = driver.findElements(maritalStatus);
        maritalStatuses.get(new Random().nextInt(maritalStatuses.size())).click();
        return this;
    }

    public RegistrationPage selectRandomHobie(int hobbiesNumber) {
        List<WebElement> hobbies = driver.findElements(hobby);
        hobbies.get(hobbiesNumber).click();
        return this;
    }

    public RegistrationPage selectRandomCountry() {
        Select countrySelector = new Select(driver.findElement(country));
        List<WebElement> countries = countrySelector.getOptions();
        countrySelector.selectByIndex(new Random().nextInt(countries.size()));
        return this;
    }

    public RegistrationPage selectDateOfBirth(String fullDateOfBirth) {
        if (!fullDateOfBirth.matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {
            throw new IllegalArgumentException("Date of birth should be in format YYYY-MM-DD or YYYY-M-D ");
        }
        String[] parts = fullDateOfBirth.split("-");
        selectBirthYear(parts[0]).selectBirthMonth(parts[1]).selectBirthDay(parts[2]);
        return this;
    }

    public RegistrationPage selectBirthMonth(String monthValue) {
        Select select = new Select(monthOfBirth);
        if (monthValue.startsWith("0")) {
            monthValue = monthValue.substring(1);
        }
        select.selectByVisibleText(monthValue);
        return this;
    }

    public RegistrationPage selectBirthDay(String dayNumber) {
        Select select = new Select(dayOfBirth);
        if (dayNumber.startsWith("0")) {
            dayNumber = dayNumber.substring(1);
        }
        select.selectByVisibleText(dayNumber);
        return this;
    }

    public RegistrationPage selectBirthYear(String birthYearValue) {
        Select select = new Select(yearOfBirth);
        select.selectByValue(birthYearValue);
        return this;
    }

    public RegistrationPage enterPhoneNumber(String phoneNumberValue) {
        phoneNumber.sendKeys(phoneNumberValue);
        return this;
    }

    public RegistrationPage enterUsername(String usernameValue) {
        username.sendKeys(usernameValue);
        return this;
    }

    public RegistrationPage enterEmail(String emailValue) {
        email.sendKeys(emailValue);
        return this;
    }

    public RegistrationPage enterPassword(String passwordValue) {
        password.sendKeys(passwordValue);
        return this;
    }

    public RegistrationPage confirmPassword(String passwordValue) {
        confirmPassword.sendKeys(passwordValue);
        return this;
    }

    public RegistrationPage submitRegistrationForm() {
        submit.click();
        return this;
    }
    public void waitForRegistrationSuccessMassageToBeDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(registrationSuccessMessage));
    }
    public boolean isRegistrationSuccessMessageDisplayed() {
        return registrationSuccessMessage.isDisplayed();
    }
}

