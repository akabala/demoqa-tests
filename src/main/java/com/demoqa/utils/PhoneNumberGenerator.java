package com.demoqa.utils;

import java.util.Random;

public class PhoneNumberGenerator {

    public String getRandomNumber(int length) {
        final String SALTCHARS = "1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }
}
