package com.demoqa.utils;

import java.util.Random;

public class EmailGenerator {

    public String generateRandomEmail(int length) {
        return getSaltString(length) + "@gmail.com";
    }

    private String getSaltString(int length) {
        final String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();
    }
}
